# **Hawk**

![license](http://img.shields.io/badge/license-Apache2.0-brightgreen.svg)
![Release Version](https://img.shields.io/badge/release-1.0.0-yellow.svg)

> Android 简单数据存储工具

&ensp;&ensp;现阶段支持除Map,Set的Object,List直接存储和读取。数据存放在SharedPreferences中。之后维护会添加更多Storage,并逐步改正现阶段残留问题。

## **使用方法**

1. 在Application中进行初始化
   Hawk.init(CONTEXT).build();

2. 在需要存放数据处
   Hawk.put(KEY, VALUE);

3. 在需要读取数据处
   Hawk.get(KEY)

** 注意事项: 
    put与get方法是绑定使用的。put的过程中会将数据转换编码加密，因此如果想使用原生的数据存储的可使用Hawk.putWithoutEncrypt(KEY, VALUE)方法，对应读取的方法为Hawk.getWithoutDecrypted(KEY)


## **提醒**

1. Lib中封装了对[Timber](https://github.com/JakeWharton/timber)及[Conceal](https://github.com/facebook/conceal)的使用，如果工程师使用了其中的库，Lib中会自动使用


## **遗留问题**

1. 暂时需要FastJson支持


## **LICENSE**

    Copyright 2018 Tom.Shen

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
