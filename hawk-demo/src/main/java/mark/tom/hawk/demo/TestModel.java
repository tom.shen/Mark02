package mark.tom.hawk.demo;

import java.util.List;

public class TestModel {

    private List<String> fileNames;

    private String decryptKey;

    public List<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public String getDecryptKey() {
        return decryptKey;
    }

    public void setDecryptKey(String decryptKey) {
        this.decryptKey = decryptKey;
    }

    @Override
    public String toString() {
        return "TestModel{" +
                "fileNames=" + fileNames +
                ", decryptKey='" + decryptKey + '\'' +
                '}';
    }
}
