package mark.tom.hawk.demo;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mark.tom.hawk.Hawk;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView textView = findViewById(R.id.textView);

        StringBuilder sb = new StringBuilder();
        sb.append("即将执行以下语句: \n");
        sb.append("TestModel obj = new TestModel();\n" +
                "obj.setDecryptKey(\"test\");\n" +
                "List<TestModel> list = new ArrayList<TestModel>();\n" +
                "for (int i = 0; i < 10; i++) {\n" +
                "    TestModel tmp = new TestModel();\n" +
                "    tmp.setDecryptKey(String.valueOf(i));\n" +
                "    list.add(tmp);\n" +
                "}\n" +
                "Hawk.put(\"String\", \"string\");\n" +
                "Hawk.put(\"Integer\", 1);\n" +
                "Hawk.put(\"Object\", obj);\n" +
                "Hawk.put(\"List\", list);");
        sb.append("\n\n");

        Timber.i("=========================Hawk Write Begin======================================");
        TestModel obj = new TestModel();
        obj.setDecryptKey("test");

        List<TestModel> list = new ArrayList<TestModel>();
        for (int i = 0; i < 10; i++) {
            TestModel tmp = new TestModel();
            tmp.setDecryptKey(String.valueOf(i));
            list.add(tmp);
        }
        Hawk.put("String", "string");
        Hawk.put("Integer", 1);
        Hawk.put("Object", obj);
        Hawk.put("List", list);
        Timber.i("=========================Hawk Write End======================================");

        sb.append("使用Hawk.getWithoutDecrypted()查看数据存放在SharedPreferences中的格式 : \n");
        sb.append("Key : String \n").append("Data : ").append(Hawk.getWithoutDecrypted("String")).append("\n\n");
        sb.append("Key : Integer \n").append("Data : ").append(Hawk.getWithoutDecrypted("Integer")).append("\n\n");
        sb.append("Key : Object \n").append("Data : ").append(Hawk.getWithoutDecrypted("Object")).append("\n\n");
        sb.append("Key : List \n").append("Data : ").append(Hawk.getWithoutDecrypted("List")).append("\n\n");

        sb.append("使用Hawk.get()查看数据解密后的格式 : \n");
        sb.append("Key : String \n").append("Data : ").append(Hawk.get("String").toString()).append("\n\n");
        sb.append("Key : Integer \n").append("Data : ").append(Hawk.get("Integer").toString()).append("\n\n");
        sb.append("Key : Object \n").append("Data : ").append(Hawk.get("Object").toString()).append("\n\n");
        sb.append("Key : List \n").append("Data : ").append(Hawk.get("List").toString()).append("\n\n");

        textView.setText(sb.toString());
    }
}
