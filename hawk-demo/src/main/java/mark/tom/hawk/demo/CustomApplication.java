package mark.tom.hawk.demo;

import android.app.Application;

import mark.tom.hawk.ConcealEncryption;
import mark.tom.hawk.FastJsonParser;
import mark.tom.hawk.Hawk;
import mark.tom.hawk.TimberLogger;
import timber.log.Timber;

public class CustomApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Init Timber
        Timber.plant(new Timber.DebugTree());
        // Init Hawk
        Hawk.init(this)
                .setEncryption(new ConcealEncryption(this.getApplicationContext()))
                .setLogger(new TimberLogger())
                .setParser(new FastJsonParser())
                .build();
    }
}
