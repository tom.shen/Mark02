package mark.tom.hawk;

import androidx.annotation.NonNull;
import mark.tom.hawk.template.Logger;
import timber.log.Timber;

/**
 * @Description: Timber实现的日志操作类
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk
 * @Date: 2020/9/18
 * @Copyright: 版权归 Tom.Shen 所有
 */
public final class TimberLogger extends Logger {

    private final static String TAG = "TimberLogger";

    @Override
    public void onOff(final boolean on) {
        this.onOff = on;
    }

    @Override
    public void log(@NonNull final String message, @LogPriority final int level) {
        if (!onOff) {
            return;
        }
        Timber.tag(TAG).log(level, message);
    }
}
