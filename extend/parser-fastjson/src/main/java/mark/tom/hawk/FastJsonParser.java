package mark.tom.hawk;

import com.alibaba.fastjson.JSON;

import androidx.annotation.NonNull;
import mark.tom.hawk.template.Parser;

/**
 * @Description: 使用FastJson的数据解析器
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk
 * @Date: 2020/9/26
 * @Copyright: 版权归 Tom.Shen 所有
 */
public final class FastJsonParser implements Parser {

    @SuppressWarnings("unchecked")
    @Override
    public <T> T fromJson(@NonNull String content, @NonNull Class<?> type, @DataKind int kind) {
        T result = null;
        switch (kind) {
            case TYPE_OBJECT : {
                result = (T) JSON.parseObject(content, type);
                break;
            }
            case TYPE_LIST : {
                result = (T) JSON.parseArray(content, type);
                break;
            }
            default: {
                // Ignore
            }
        }
        return result;
    }

    @Override
    public String toJson(@NonNull Object body) {
        return JSON.toJSONString(body);
    }

}
