package mark.tom.hawk;

import android.util.Base64;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import mark.tom.hawk.template.Encryption;

/**
 * @Description: Base64加密接口实现
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk
 * @Date: 2020/9/18
 * @Copyright: 版权归 Tom.Shen 所有
 */
public class Base64Encryption implements Encryption {

    @Override
    public boolean hasInit() {
        return true;
    }

    @Override
    public String encrypt(@Nullable String key, @NonNull String value) {
        return encodeBase64(value.getBytes());
    }

    @Override
    public String decrypt(@Nullable String key, @NonNull String value) {
        return new String(decodeBase64(value));
    }

    private String encodeBase64(byte[] bytes) {
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    private byte[] decodeBase64(String value) {
        return Base64.decode(value, Base64.DEFAULT);
    }
}
