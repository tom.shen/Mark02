package mark.tom.hawk;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.annotation.NonNull;
import mark.tom.hawk.template.Logger;

/**
 * @Description: Android实现的日志操作类
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk
 * @Date: 2020/9/18
 * @Copyright: 版权归 Tom.Shen 所有
 */
class AndroidLogger extends Logger {

    private final static String TAG = "AndroidLogger";

    @Override
    public void onOff(final boolean on) {
        this.onOff = on;
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void log(@NonNull final String message, @LogPriority final int level) {
        if (!onOff) {
            return;
        }
        Log.println(level, TAG, message);
    }
}
