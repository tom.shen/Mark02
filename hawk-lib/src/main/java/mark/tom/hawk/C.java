package mark.tom.hawk;

final class C {

    // 默认SharedPreferences文件名
    final static String DEFAULT_SHARED_PREFERENCES_NAME = "Hawk";
    // 默认使用SharedPreferences标签
    final static String DEFAULT_SP_TAG = SharedPreferencesStorage.STORAGE_SP_TAG;
}
