package mark.tom.hawk;

import android.content.Context;
import androidx.annotation.NonNull;

import mark.tom.hawk.template.Facade;

public final class Hawk {

    private static Facade facade = new Facade.EmptyHawkFacade();

    public static HawkBuilder init(@NonNull Context context) {
        Util.checkNull("Context", context);
        facade = null;
        return new HawkBuilder(context);
    }

    static void build(HawkBuilder hawkBuilder) {
        facade = new HawkFacade(hawkBuilder);
    }

    public static boolean hasBuild() {
        return facade.hasBuild();
    }

    public static void destroy() {
        facade.destroy();
    }

    // ****************************************普通的存与读**************
    public static <T> boolean put(@NonNull String key, @NonNull T value) {
        return facade.put(key, value);
    }

    public static <T> boolean put(@NonNull String key, @NonNull T value, @NonNull String... tags) {
        return facade.put(key, value, tags);
    }

    public static <T> T get(@NonNull String key) {
        return facade.get(key);
    }

    public static <T> T get(@NonNull String key, @NonNull String tag) {
        return facade.get(key, tag);
    }
    // ****************************************************************

    // ****************************************存放上层传来的数据(不进行任何加工)**************
    public static boolean putWithoutEncrypt(@NonNull String key, @NonNull String encryptedValue) {
        return facade.saveEncryptRecord(key, encryptedValue);
    }

    public static boolean putWithoutEncrypt(@NonNull String key, @NonNull String encryptRecord, @NonNull String... tags) {
        return facade.saveEncryptRecord(key, encryptRecord, tags);
    }

    public static String getWithoutDecrypted(@NonNull String key) {
        return facade.obtainEncryptRecord(key);
    }

    public static String getWithoutDecrypted(@NonNull String key, @NonNull String tag) {
        return facade.obtainEncryptRecord(key, tag);
    }
    // ****************************************************************************************

    public static long count() {
        return facade.count();
    }

    public static long count(@NonNull String tag) {
        return facade.count(tag);
    }

    public static boolean delete(@NonNull String key) {
        return facade.delete(key);
    }

    public static boolean delete(@NonNull String key, @NonNull String tag) {
        return facade.delete(key, tag);
    }

    public static boolean deleteAll() {
        return facade.deleteAll();
    }

    public static boolean deleteAll(@NonNull String tag) {
        return facade.deleteAll(tag);
    }

    public static boolean contains(@NonNull String key) {
        return facade.contains(key);
    }

    public static boolean contains(@NonNull String key, @NonNull String tag) {
        return facade.contains(key, tag);
    }

}
