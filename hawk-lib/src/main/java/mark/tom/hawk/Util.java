package mark.tom.hawk;

/**
 * @Description: 内部工具类
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk
 * @Date: 2020/9/18
 * @Copyright: 版权归 Tom.Shen 所有
 */
final class Util {

    /**
     * Description: 判断输入值是否为null
     * Author: Tom.Shen
     * Date: 2020/9/18 17:41
     */
    static void checkNull(String message, Object value) {
        if (value == null) {
            throw new NullPointerException(message);
        }
    }

    /**
     * Description: 判断输入字符串是否为空或null
     * Author: Tom.Shen
     * Date: 2020/9/18 17:42
     */
    static void checkNullOrEmpty(String message, String value) {
        if (isEmpty(value)) {
            throw new NullPointerException(message + " should not be null or empty");
        }
    }

    /**
     * Description: 判断输入字符串是否为空或null
     * Author: Tom.Shen
     * Date: 2020/9/18 17:42
     */
    static boolean isEmpty(String text) {
        return text == null || text.trim().length() == 0;
    }

}
