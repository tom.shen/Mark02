package mark.tom.hawk.template;

import androidx.annotation.NonNull;

/**
 * @Description: 加解密接口
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk.template
 * @Date: 2020/9/18
 * @Copyright: 版权归 Tom.Shen 所有
 */
public interface Encryption {

    /**
     * Description: 判断是否已经初始化
     * Author: Tom.Shen
     * Date: 2020/9/18 17:46
     */
    boolean hasInit();

    /**
     * Description: 加密数据
     * Author: Tom.Shen
     * Date: 2020/9/18 17:46
     */
    String encrypt(String key, @NonNull String value) throws Exception;

    /**
     * Description: 解密数据
     * Author: Tom.Shen
     * Date: 2020/9/18 17:46
     */
    String decrypt(String key, @NonNull String value) throws Exception;

}
