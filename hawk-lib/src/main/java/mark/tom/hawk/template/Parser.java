package mark.tom.hawk.template;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

/**
 * @Description: 数据解析器
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk.template
 * @Date: 2020/9/20
 * @Copyright: 版权归 Tom.Shen 所有
 */
public interface Parser {

    int TYPE_OBJECT = 1;       // 对象类型
    int TYPE_LIST = 2;         // 列表数据类型

    @IntDef({TYPE_OBJECT, TYPE_LIST})
    @Retention(RetentionPolicy.SOURCE)
    @interface DataKind {}

    /**
     * @Description: 将JSON文本解析成对应对象
     * @Author: Tom.Shen
     * @Date: 2020/9/26
     */
    <T> T fromJson(@NonNull String content, @NonNull Class<?> type, @DataKind int kind) throws Exception;

    /**
     * @Description: 将对象解析成JSON文本
     * @Author: Tom.Shen
     * @Date: 2020/9/26
     */
    String toJson(@NonNull Object body);

}
