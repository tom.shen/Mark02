package mark.tom.hawk.template;

import mark.tom.hawk.DataInfo;

/**
 * @Description: 转换器接口
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk.template
 * @Date: 2020/9/20
 * @Copyright: 版权归 Tom.Shen 所有
 */
public interface Converter {

    /**
     * @Description: 对象转String
     * @Author: Tom.Shen
     * @Date: 2020/9/20
     */
    <T> String toString(T value);

    /**
     * @Description: 将String装换为对象
     * @Author: Tom.Shen
     * @Date: 2020/9/20
     */
    <T> T fromString(String value, DataInfo dataInfo) throws Exception;
}
