package mark.tom.hawk.template;

/**
 * @Description: 数据存储器接口
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk.template
 * @Date: 2020/9/20
 * @Copyright: 版权归 Tom.Shen 所有
 */
public interface Storage {

    String tag();

    <T> boolean put(String key, T value);

    <T> T get(String key);

    boolean delete(String key);

    boolean deleteAll();

    long count();

    boolean contains(String key);

}
