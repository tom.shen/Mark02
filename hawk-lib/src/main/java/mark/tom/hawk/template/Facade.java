package mark.tom.hawk.template;

import androidx.annotation.NonNull;

public interface Facade {

    <T> boolean put(@NonNull String key, @NonNull T value);

    <T> boolean put(@NonNull String key, @NonNull T value, @NonNull String... tags);

    <T> String getPositiveProcessedData(@NonNull String key, @NonNull T value) throws Exception;

    boolean saveEncryptRecord(@NonNull String key, @NonNull String encryptRecord);

    boolean saveEncryptRecord(@NonNull String key, @NonNull String encryptRecord, @NonNull String... tags);

    <T> T get(@NonNull String key);

    <T> T get(@NonNull String key, @NonNull String tag);

    <T> T getReverseProcessedData(@NonNull String key, @NonNull String serializedText) throws Exception;

    String obtainEncryptRecord(@NonNull String key);

    String obtainEncryptRecord(@NonNull String key, @NonNull String tag);

    long count();

    long count(@NonNull String tag);

    boolean deleteAll();

    boolean deleteAll(@NonNull String tag);

    boolean delete(@NonNull String key);

    boolean delete(@NonNull String key, @NonNull String tag);

    boolean contains(@NonNull String key);

    boolean contains(@NonNull String key, @NonNull String tag);

    boolean hasBuild();

    void destroy();

    class EmptyHawkFacade implements Facade {

        @Override
        public <T> boolean put(@NonNull String key, @NonNull T value) {
            throwValidation();
            return false;
        }

        @Override
        public <T> boolean put(@NonNull String key, @NonNull T value, @NonNull String... tags) {
            throwValidation();
            return false;
        }

        @Override
        public <T> String getPositiveProcessedData(@NonNull String key, @NonNull T value) {
            throwValidation();
            return null;
        }

        @Override
        public boolean saveEncryptRecord(@NonNull String key, @NonNull String encryptRecord) {
            throwValidation();
            return false;
        }

        @Override
        public boolean saveEncryptRecord(@NonNull String key, @NonNull String encryptRecord, @NonNull String... tags) {
            throwValidation();
            return false;
        }

        @Override
        public <T> T get(@NonNull String key) {
            throwValidation();
            return null;
        }

        @Override
        public <T> T get(@NonNull String key, @NonNull String tag) {
            throwValidation();
            return null;
        }

        @Override
        public <T> T getReverseProcessedData(@NonNull String key, @NonNull String serializedText) {
            throwValidation();
            return null;
        }

        @Override
        public String obtainEncryptRecord(@NonNull String key) {
            throwValidation();
            return null;
        }

        @Override
        public String obtainEncryptRecord(@NonNull String key, @NonNull String tag) {
            throwValidation();
            return null;
        }

        @Override
        public long count() {
            throwValidation();
            return 0;
        }

        @Override
        public long count(@NonNull String tag) {
            throwValidation();
            return 0;
        }

        @Override
        public boolean deleteAll() {
            throwValidation();
            return false;
        }

        @Override
        public boolean deleteAll(@NonNull String tag) {
            throwValidation();
            return false;
        }

        @Override
        public boolean delete(@NonNull String key) {
            throwValidation();
            return false;
        }

        @Override
        public boolean delete(@NonNull String key, @NonNull String tag) {
            throwValidation();
            return false;
        }

        @Override
        public boolean contains(@NonNull String key) {
            throwValidation();
            return false;
        }

        @Override
        public boolean contains(@NonNull String key, @NonNull String tag) {
            throwValidation();
            return false;
        }

        @Override
        public boolean hasBuild() {
            return false;
        }

        @Override
        public void destroy() {
            throwValidation();
        }

        private void throwValidation() {
            throw new IllegalStateException("Hawk is not built. " +
                    "Please call build() and wait the initialisation finishes.");
        }
    }
}
