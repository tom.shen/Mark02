package mark.tom.hawk.template;

import android.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

/**
 * @Description: 日志抽象类
 * @Author: Tom.Shen
 * @Package: mark.tom.hawk.template
 * @Date: 2020/9/18
 * @Copyright: 版权归 Tom.Shen 所有
 */
public abstract class Logger {

    // 日志开关
    protected boolean onOff = true;

    // 日志等级枚举
    @IntDef({Log.VERBOSE, Log.DEBUG, Log.INFO, Log.WARN, Log.ERROR, Log.ASSERT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LogPriority {}

    /**
     * Description: 日志开关
     * Author: Tom.Shen
     * Date: 2020/9/18 17:46
     */
    public abstract void onOff(boolean on);

    /**
     * Description: 记录日志
     * Author: Tom.Shen
     * Date: 2020/9/18 17:46
     */
    public abstract void log(@NonNull String message, @LogPriority int level);

}
