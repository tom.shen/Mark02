package mark.tom.hawk.template;

import mark.tom.hawk.DataInfo;

public interface Serializer {

    <T> String serialize(String cipherText, T value);

    DataInfo deserialize(String plainText);
}