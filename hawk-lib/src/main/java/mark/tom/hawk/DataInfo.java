package mark.tom.hawk;

public final class DataInfo {

    static final char TYPE_OBJECT = '0';
    static final char TYPE_LIST = '1';

    final char dataType;
    final String cipherText;
    final Class<?> keyClazz;

    DataInfo(char dataType, String cipherText, Class<?> keyClazz) {
        this.cipherText = cipherText;
        this.keyClazz = keyClazz;
        this.dataType = dataType;
    }
}
